/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Scanner;
import servidor.Jogador;

public class Client {

    public static void main(String[] args) {

        try {
            String loc = "//localhost/service";
            //Procura pelo servidor para obter o serviço
            Service service = (Service) Naming.lookup(loc);
            String op = "";

            System.out.println("Informe seu nome:");
            String nome = new Scanner(System.in).nextLine();

            service.cadastrarJogador(nome);

            while (!op.equals("0")) {
                if (op.equals("")) {
                    System.out.println("(1) - Novo Jogo");
                    System.out.println("(2) - Visualizar ranking");
                    System.out.println("(3) - Submeter Pontuação");
                    System.out.println("(0) - Sair");
                }

                op = new Scanner(System.in).next();
                if (op.equals("1")) {

                    System.out.println(service.novoJogo(nome));
                    char teclado = ' ';
                    Scanner scan = new Scanner(System.in);
                    System.out.println(service.mostrarTentativas());
                    do {

                        System.out.println("(1) - Advinhar Letra");
                        System.out.println("(2) - Adivinhar frase - tudo ou nada");
                        System.out.println("(3) - Pedir dica");
                        System.out.println("(0) - Voltar ao Menu");

                        teclado = scan.next().charAt(0);

                        if (teclado == '1') {
                            System.out.println("Informe uma letra: ");
                            char informaLetra = new Scanner(System.in).next().charAt(0);

                            String msg = service.advinharLetra(informaLetra);
                            String decifraMsg[] = new String[2];
                            decifraMsg = msg.split("#");

                            System.out.println(decifraMsg[1]);
                            System.out.println(service.mostraForca());
                            System.out.println(service.mostrarTentativas() + " " + service.mostrarPontuacao());
                            if (decifraMsg[0].equals("0")) {
                                teclado = '0';
                                op = "";
                            } else if (decifraMsg[0].equals("1")) {
                                teclado = '0';
                                op = "";
                            }

                        } else if (teclado == '2') {
                            System.out.println("Informe a frase: ");
                            String informaFrase = new Scanner(System.in).nextLine();

                            System.out.println(service.advinharFrase(informaFrase));

                            System.out.println(service.mostrarTentativas() + " " + service.mostrarPontuacao());
                            teclado = '0';
                            op = "";

                        } else if (teclado == '3') {
                            System.out.println(service.solicitarDica());
                            System.out.println(service.mostraForca());
                        } else if (teclado == '0') {
                            teclado = '0';
                            op = "";
                        }
                    } while (teclado != '0');
                }
                if (op.equals("2")) {
                    System.out.println(service.verRanking());
                    op = "";
                }
                if (op.equals("3")) {
                    service.cadastrarPontuacao(nome);
                    op = "";
                }
            }

            //teclado.close();
        } catch (MalformedURLException | RemoteException | NotBoundException e) {
            e.printStackTrace();
        }

    }

}
