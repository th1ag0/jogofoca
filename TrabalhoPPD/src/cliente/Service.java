/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import servidor.Jogador;

public interface Service extends Remote {

	public String novoJogo(String nome) throws RemoteException;
        public String mostraForca() throws RemoteException;
        public String solicitarDica() throws RemoteException;
        public String verRanking() throws RemoteException;
        public void cadastrarJogador(String nome) throws RemoteException;
        public void cadastrarPontuacao(String nome) throws RemoteException;
        public String mostrarPontuacao() throws RemoteException;
        public String mostrarTentativas() throws RemoteException;
        public String advinharLetra(char letra)throws RemoteException;
        public String advinharFrase(String frase) throws RemoteException;
}
