/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.util.Comparator;

/**
 *
 * @author thiago
 */
public class Jogador implements Comparable<Jogador> {

    private String nome;
    private int qtdeJogos;
    private int TotalPontos;

    public Jogador(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdeJogos() {
        return qtdeJogos;
    }

    public void setQtdeJogos() {
        this.qtdeJogos++;
    }

    public int getTotalPontos() {
        return TotalPontos;
    }

    public void setTotalPontos(int TotalPontos) {
        this.TotalPontos += TotalPontos;
    }

    @Override
    public int compareTo(Jogador o) {
        if (this.getTotalPontos() > o.getTotalPontos()) {
            return -1;
        } else if (this.getTotalPontos() < o.getTotalPontos()) {
            return +1;
        } else {
            return 0;
        }
    }

    public static Comparator<Jogador> getComparatorCresc() {
        return new Comparator<Jogador>() {
            @Override
            public int compare(Jogador o1, Jogador o2) {
                if (o1.getTotalPontos() > o2.getTotalPontos()) {
                    return -1;
                } else if (o1.getTotalPontos() < o2.getTotalPontos()) {
                    return +1;
                } else {
                    return 0;
                }
            }
        };
    }
}
