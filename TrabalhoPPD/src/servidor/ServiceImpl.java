/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import cliente.Service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class ServiceImpl extends UnicastRemoteObject implements Service {

    private Jogo jogo;
    private ArrayList<String> frasesDoJogo;
    private ArrayList<String> dicasDoJogo;
    private ArrayList<Jogador> ranking;
    private Jogador jogador;

    protected ServiceImpl() throws RemoteException {
        super();
        frasesDoJogo = new ArrayList<>();
        dicasDoJogo = new ArrayList<>();
        ranking = new ArrayList<>();
        cadastrarDadosJogo();
    }

    private static final long serialVersionUID = 1L;

    @Override
    public String novoJogo(String nome) throws RemoteException {
        int indice = new Random().nextInt(10);
        this.jogo = new Jogo(frasesDoJogo.get(indice), dicasDoJogo.get(indice));
        char[] letras = jogo.getFrase().toCharArray();

        for (int i = 0; i < ranking.size(); i++) {
             if (ranking.get(i).getNome().equals(nome)) {
                ranking.get(i).setQtdeJogos();
             }
            
        }
        String palavra = "";
        for (int i = 0; i < letras.length; i++) {

            if (letras[i] == ' ') {
                palavra += " ";
            } else if (letras[i] == ',') {
                palavra += ",";
            } else if (letras[i] == '?') {
                palavra += "?";
            } else if (letras[i] == '.') {
                palavra += ".";
            } else if (letras[i] == ';') {
                palavra += ":";
            } else if (letras[i] == '!') {
                palavra += "!";
            } else if (letras[i] == ':') {
                palavra += ":";
            } else if (letras[i] == '’') {
                palavra += "’";
            } else {
                palavra += "-";
            }
        }
        jogo.setFraseTeste(palavra);
        System.out.println(palavra);
        return palavra;

    }

    @Override
    public String solicitarDica() throws RemoteException {
        return "Dica: " + jogo.getDica();
    }

    @Override
    public String advinharLetra(char letra) throws RemoteException {

        char[] letras = jogo.getFrase().toCharArray();
        char[] montarIncompletas = jogo.getFraseTeste().toCharArray();
        boolean acerto = false;
        System.out.println(jogo.getFraseTeste());
        String frase = "";

        for (int i = 0; i < letras.length; i++) {

            if (letras[i] == letra) {
                frase += letra;

                acerto = true;
            } else {

                frase += montarIncompletas[i];
            }
        }
        jogo.setFraseTeste(frase);

        String msg = "";
        if (frase.equals(jogo.getFrase())) {
            msg = "1#Parabéns, você completou a frase!";
        } else if (!acerto) {
            jogo.subPontos(1);
            jogo.setTentativas();
            if (jogo.getTentativas() < 1) {
                msg = "0#Você Perdeu!";
            } else {
                msg = "3#Errou!";
            }
        } else {
            msg = "4#Acertou!";
            jogo.addPontos(1);
        }

        return msg;
    }

    @Override
    public String advinharFrase(String frase) throws RemoteException {
        String fraseDoJogo = jogo.getFrase();
        int cont = 0;
        String msg = "";
        char[] letras = jogo.getFrase().toCharArray();
        char[] montarIncompletas = jogo.getFraseTeste().toCharArray();
        if (frase.equals(fraseDoJogo)) {

            for (int i = 0; i < montarIncompletas.length; i++) {
                if (montarIncompletas[i] == '-') {
                    System.out.println(letras[i]);
                    cont++;
                }
            }

            jogo.addPontos(cont * 2);
            msg = "\nParabéns, você completou a frase!";
        } else {
            jogo.zerarPontos();
            msg = "\nErrou!";
        }

        return msg;
    }

    @Override
    public String mostrarTentativas() throws RemoteException {
        return "Tentativas: " + jogo.getTentativas();
    }

    @Override
    public String mostrarPontuacao() throws RemoteException {
        return "Pontos: " + jogo.getPontos();
    }

    @Override
    public String mostraForca() throws RemoteException {
        return jogo.getFraseTeste();
    }

    private void cadastrarDadosJogo() {
        frasesDoJogo.add("Why so serious?");
        dicasDoJogo.add("Batman, O Cavaleiro das Trevas");

        frasesDoJogo.add("My name is Bond, James Bond");
        dicasDoJogo.add("007");

        frasesDoJogo.add("I’m the King of the world");
        dicasDoJogo.add("Titanic");

        frasesDoJogo.add("Hasta la vista, baby ");
        dicasDoJogo.add("Exterminador do Futuro 2");

        frasesDoJogo.add("Luke, I am your father");
        dicasDoJogo.add("Star Wars – O império contra-ataca");

        frasesDoJogo.add("I see dead people");
        dicasDoJogo.add("O Sexto Sentido");

        frasesDoJogo.add("Houston, we have a problem");
        dicasDoJogo.add("Apollo");

        frasesDoJogo.add("Let the games begin");
        dicasDoJogo.add("Jogos Mortais");

        frasesDoJogo.add("Elementary, my dear Watson");
        dicasDoJogo.add("Sherlock Holmes");

        frasesDoJogo.add("Run Forest, Run");
        dicasDoJogo.add("Forest Gump");
    }

    @Override
    public void cadastrarPontuacao(String nome) throws RemoteException {
        for (int i = 0; i < ranking.size(); i++) {
            if (ranking.get(i).getNome().equals(nome)) {
                ranking.get(i).setTotalPontos(jogo.pontos);
            }
            
        }
        
        System.out.println(jogador.getNome());
        System.out.println(jogador.getTotalPontos());
    }

    @Override
    public void cadastrarJogador(String nome) throws RemoteException {
        jogador = new Jogador(nome);

        ranking.add(jogador);

    }

    @Override
    public String verRanking() throws RemoteException {
        ArrayList<Jogador> jog = ranking;
        Collections.sort(jog, Jogador.getComparatorCresc());
        String str = "";
        int indice = 1;
        for (int i = 0; i < jog.size(); i++) {
            str += indice + "º: " + jog.get(i).getNome() + "Pontuação: " + jog.get(i).getTotalPontos() + "Nº de Jogos: "
                    + jog.get(i).getQtdeJogos() + "\n";
            indice++;
        }
        return str;
    }
}
