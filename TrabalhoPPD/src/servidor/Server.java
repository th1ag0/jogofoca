/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Server {

    public static void main(String[] args) {
        try {
            //Cria um novo serviço
            ServiceImpl service = new ServiceImpl();

            //Define o nome do objeto servidor que será utilizado por clientes
            String loc = "//localhost/service";

            //Registra o serviço no servidor, passando por parâmetro 
            //o nome do serviço a ser disponibilizado, e o objeto remoto
            Naming.rebind(loc, service);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
