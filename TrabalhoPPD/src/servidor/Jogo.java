/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.util.ArrayList;

/**
 *
 * @author thiago
 */
public class Jogo {
    String fraseTeste;
    String frase;
    String dica;
    int tentativas;
    int qtdeLetras;
    int pontos;
    
    ArrayList<String> frases;
    ArrayList<String> dicas;

    public Jogo(String frase, String dica) {
        this.frase = frase;
        this.dica=dica;
        this.tentativas = frase.length()/2;
        int tentativas=0;
        
        frases = new ArrayList<>();
        dicas = new ArrayList<>();
               
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }

    public String getDica() {
        return dica;
    }

    public String getFraseTeste() {
        return fraseTeste;
    }

    public void setFraseTeste(String fraseTeste) {
        this.fraseTeste = fraseTeste;
    }

    public void setDica(String dica) {
        this.dica = dica;
    }

    public int getTentativas() {
        return tentativas;
    }

    public void setTentativas() {
        this.tentativas--;
    }

    public int getQtdeLetras() {
        return qtdeLetras;
    }

    public void setQtdeLetras(int qtdeLetras) {
        this.qtdeLetras = qtdeLetras;
    }

    public int getPontos() {
        return pontos;
    }

    public void addPontos(int ponto) {
        this.pontos += ponto;
    }
    public void subPontos(int ponto) {
        this.pontos -= ponto;
    }    
    public void zerarPontos(){
        this.pontos = 0;
    }
    
}
